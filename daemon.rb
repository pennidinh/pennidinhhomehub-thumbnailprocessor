#!/usr/bin/env ruby

puts "loading libraries..."
require 'net/http'
require 'json'
require 'cgi'
require 'uri'
require 'fileutils'

def buildRequest(uri)
  return Net::HTTP::Post.new(URI(uri))
end

def deleteOriginalFile(originalFile)
  req = buildRequest('http://' + ENV['NODE_SERVER_HOSTNAME'] + ':' + ENV['NODE_SERVER_PORT'] + '/deleteFile.js?file=' + CGI::escape(originalFile))
  response = Net::HTTP.start(ENV['NODE_SERVER_HOSTNAME'], ENV['NODE_SERVER_PORT'].to_i) do |http|
    puts "Requesting delte of corrupt file: #{originalFile}"
    $stdout.flush
    http.request req 
  end
  if response.code == '200'
    puts "Delete successful"
  else
    puts "Delete failed"
  end
end

def uploadThumbnail(originalFile, localFile, thumbnailSuffix)
  puts "Persisting thumbnail for #{originalFile} from #{localFile} with thumbnail suffix #{thumbnailSuffix}"

  FileUtils.copy_file(localFile, originalFile[0..(originalFile.rindex('.') - 1)] + thumbnailSuffix)
end

def startDaemon
  puts "daemon starting up..."
  while true do
    Dir.glob('/tmp/thumbnail-*').each { |file| File.delete(file)}

    puts Time.new.inspect
    $stdout.flush
    begin
      req = buildRequest 'http://' + ENV['NODE_SERVER_HOSTNAME'] + ':' + ENV['NODE_SERVER_PORT'] + '/popVideoThumbnail.js'
      response = Net::HTTP.start(ENV['NODE_SERVER_HOSTNAME'], ENV['NODE_SERVER_PORT'].to_i) do |http|
        http.read_timeout = 130
        puts "Starting blocking request to get next thumbnail..."
        $stdout.flush
        http.request req 
      end
      if response.code == '200'
        originalFile = CGI::unescape(response.body)
        
        if originalFile.include? 'thumbnail'
  	 next
        end
  
        puts "File to generate thumbnail for exists: #{originalFile}"
  
        if originalFile.include?("mp4") || originalFile.include?('MP4') || originalFile.include?('mov') || originalFile.include?('MOV')   
          thumbnailAlreadyExists = Dir.glob(originalFile[0..(originalFile.rindex('.') - 1)] + '*').any? { |file| file =~ /-thumbnail-([0-9]+)\.png$/ }
          if thumbnailAlreadyExists
            puts 'Video thumbnail already exists. Skipping...'
            next
          end

          # subtract one because there are issues below with the 3rd thumbnail when the video duration is close to the offset (~0.6 seconds or less)
          videoThumbnailLengthSeconds = `ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "#{originalFile}"`.to_f - 1
          puts "result for fetching seconds :#{videoThumbnailLengthSeconds}"
          videoThumbnailLengthSeconds = (videoThumbnailLengthSeconds - (videoThumbnailLengthSeconds / 30)).to_i
          puts "Video thumnail length in seconds :#{videoThumbnailLengthSeconds}"
          if videoThumbnailLengthSeconds <= 0
            puts "Corrupt original file (when checking for duration)"
  	    deleteOriginalFile originalFile
            next
          end

  	  puts "Generating video thumbnails..."
          currentSeconds = videoThumbnailLengthSeconds / 3
          [1,2,3].each { |num|
            minutes = (currentSeconds / 60).to_i
            seconds = (currentSeconds % 60).to_i
            thumbnailFile = "/tmp/thumbnail-#{num.to_s}.png"
            output = `ffmpeg -ss 00:#{minutes.to_s.rjust(2, "0")}:#{seconds.to_s.rjust(2, "0")} -i "#{originalFile}" -vframes 1 -vf "scale=260:-1"  #{thumbnailFile}`
            puts output
      	    if !File.exists?(thumbnailFile)
    	      puts "Thumbnail not created. Assumping corrupt original file"
    	      deleteOriginalFile originalFile
    	      next
            end
            currentSeconds = currentSeconds + (videoThumbnailLengthSeconds / 3)
          }
  	  puts "Thumbnail generation complete"

  	  localThumbnailFileName1 = "/tmp/thumbnail-1.png"
  	  localThumbnailFileName2 = "/tmp/thumbnail-2.png"
  	  localThumbnailFileName3 = "/tmp/thumbnail-3.png"
  	  uploadThumbnail originalFile, localThumbnailFileName3, '-thumbnail-00003.png'
  	  uploadThumbnail originalFile, localThumbnailFileName2, '-thumbnail-00002.png'
  	  uploadThumbnail originalFile, localThumbnailFileName1, '-thumbnail-00001.png'
        elsif originalFile.include?("jpg") || originalFile.include?("JPG") || originalFile.include?("jpeg") || originalFile.include?("JPEG")  || originalFile.include?("png")  || originalFile.include?("PNG")
          thumbnailAlreadyExists = Dir.glob(originalFile[0..(originalFile.rindex('.') - 1)] + '*').any? { |file| file =~ /-thumbnail\.png$/ }
          if thumbnailAlreadyExists
            puts 'Image thumbnail already exists. Skipping...'
            next
          end

          puts "Generating image thumbnails"
          typee = if (originalFile.include?("jpg") || originalFile.include?("JPG") || originalFile.include?("jpeg") || originalFile.include?("JPEG")) then 'jpg' else 'png' end
          localThumbnailFileName = "/tmp/thumbnail.png"
          output = `convert -resize 260x144 "#{typee}:#{originalFile}" png:#{localThumbnailFileName} 2>&1`
          puts 'output: ' + output
          if output.include?('insufficient image data in file')
            puts "Corrupt original image file"
            deleteOriginalFile originalFile
            next
          end
          puts "Thumbnail generation complete"
  
          uploadThumbnail originalFile, localThumbnailFileName, '-thumbnail.png'
          File.delete(localThumbnailFileName)
        else
          raise "Unexpected filetype (not mp4 for jpg) for file: #{originalFile}"
        end
      else
        puts 'No thumbnail available'
        puts 'Sleeping .010 seconds...'
        $stdout.flush
        sleep 0.010
      end
    rescue Exception => e 
      puts "Error while executing: " + e.to_s + e.backtrace.to_s
      puts 'Sleeping 1 second...'
      $stdout.flush
      sleep 1 
    end
  end
end

startDaemon
